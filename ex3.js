'use strict';

var resultDiv = document.getElementById("result-div");
var result = document.getElementById("result");

result.innerText = 'Hello!';

function onClick() {
    if (resultDiv.style.display === 'none') {
        resultDiv.style.display = 'block';
    } else {
        resultDiv.style.display = 'none';
    }
}