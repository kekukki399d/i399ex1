'use strict';

$.get('/api/persons/2948', function(data) {

    var name = data.name;
    var code = data.code;

    $.get('/api/health-insurance/' + code, function(data) {

        var isInsured = data.isInsured;

        $.get('/api/tax-debt/' + code, function(data) {

            var debt = data.debt;

            var person = {
                name: name,
                isInsured: isInsured,
                taxDebt: debt
            };

            console.log(person);

        });

    });

});
