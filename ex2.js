'use strict';

function Person(name) {

    this.name = name;

};

Person.prototype.getName = function () {
    return this.name;
};

Person.prototype.setName = function (name) {
    return this.name = name;
};


var person = new Person('Jack');
var person2 = new Person('Jill');

console.log(person.getName());
console.log(Object.keys(person));
console.log(Object.keys(person.__proto__));