'use strict';

var array = [1, 2, 3, 4, 5, 6];

array.push(7);
array.push(8);

array = array.filter(function (word) {
    return word % 2 === 0;
}).map(function (word) {
        return word * word;
    }
);

console.log('Even numbers squared: ' + array);

